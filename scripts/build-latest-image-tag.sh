#!/usr/bin/env bash

set -e -o xtrace

# Load command-line arguments
if [[ $# != 4 ]]; then
  printf 'Usage: %s DOCFILES SRCFILES SCHEME TLMIRRORURL\n' "$0" >&2
  exit 1
fi

DOCFILES="$1"
SRCFILES="$2"
SCHEME="$3"
TLMIRRORURL="$4"

# Construct temporary image tag which will be used to identify the image
# locally. As we extract the TL release's year from the image, we cannot
# construct the final tags yet.
SUFFIX="$(if [[ "$DOCFILES" = "yes" ]]; then echo "-doc"; fi)"
SUFFIX="$SUFFIX$(if [[ "$SRCFILES" = "yes" ]]; then echo "-src"; fi)"
LATESTTAG="latest-$SCHEME$SUFFIX"

# Build and temporarily tag image
# shellcheck disable=SC2068
docker build -f Dockerfile --tag "$LATESTTAG" \
  --build-arg DOCFILES="$DOCFILES" \
  --build-arg SRCFILES="$SRCFILES" \
  --build-arg SCHEME="$SCHEME" \
  --build-arg TLMIRRORURL="$TLMIRRORURL" .

# Extract the current year from the container by checking which TL year folder
# can be found in `/usr/local/texlive`.
docker run "$LATESTTAG" \
  find /usr/local/texlive/ -mindepth 1 -maxdepth 1 -type d -regex ".*/[0-9]*" -printf "%f\n" >find_output
CURRENTRELEASE=$(head -c 4 <find_output)

if ! [[ $CURRENTRELEASE =~ ^[0-9]+$ ]]; then
  echo "TeX Live release must only contain digits 0-9, invalid output $(cat find_output)." >&2
  exit 1
fi
if [ "${#CURRENTRELEASE}" -ne 4 ]; then
  echo "Years have to be represented by 4 digits, invalid output $(cat find_output)." >&2
  exit 1
fi

# Compose tags which will be pushed to the remote hosts. Then tag the image
# identified by the temporary tag with the appropriate remote tags.
IMAGEDATE="$(date +%Y-%m-%d)"
IMAGETAG="TL$CURRENTRELEASE-$IMAGEDATE-$SCHEME$SUFFIX"

# Update CI badge
#curl "https://img.shields.io/badge/latest-$IMAGETAG-blue" -o "latest-$DOCFILES-$SRCFILES-$SCHEME.svg"
